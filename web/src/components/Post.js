import React, {useEffect, useState} from 'react';

import {Avatar, Card, Grid, Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {LocalOffer} from '@material-ui/icons';

const useStyles = makeStyles({
    avatar: {
        height: 40,
        marginRight: 8,
        width: 40
    },
    post: {
        backgroundColor: 'white',
        margin: '10px 0',
        padding: '20px 30px 30px 30px'
    },
    postImage: {
        height: '100px',
        width: '200px'
    }
});

const Post = (props) => {
    const classes = useStyles();

    const {author, post} = props;

    const parseTimeString = (string) => {
        const d = new Date(string);
        return d.toLocaleString();
    };

    return (
        <Grid className={classes.post} container item xs={10}>
            <Grid container item justify='space-between' xs={12}>
                <Grid container direction='column' item xs={6}>
                    <Grid item>
                        <Typography style={{fontFamily: 'Raleway', fontSize: '30px', fontWeight: 800}}>
                            {post.title}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography style={{color: 'grey', fontFamily: 'Raleway', fontSize: 12}}>
                            {
                                `created ${parseTimeString(post.created_at)}`
                            }
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography style={{color: 'grey', fontFamily: 'Raleway', fontSize: 12}}>
                            {
                                `updated ${parseTimeString(post.updated_at)}`
                            }
                        </Typography>
                    </Grid>
                </Grid>
                <Grid alignItems='center' container item justify='flex-end' xs={3}>
                    <Grid item>
                        <Avatar className={classes.avatar} src={author.avatar}/>
                    </Grid>
                    <Grid alignItems='flex-start' container direction='column' item style={{width: 'fit-content'}}>
                        <Grid item>
                            <Typography style={{fontFamily: 'Raleway', fontSize: 14, fontWeight: 800, height: 18}}>
                                {author.name}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography component='div'
                                        style={{fontFamily: 'Raleway', fontSize: 12, fontWeight: 300, height: 14}}>
                                {author.role}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography component='div'
                                        style={{fontFamily: 'Raleway', fontSize: 12, fontWeight: 300, height: 14}}>
                                {author.location}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid alignContent='center' container item spacing={0} style={{margin: '12px 0 22px 0'}} xs={12}>
                <Grid item style={{marginRight: 6}}>
                    <LocalOffer style={{fontSize: 14}}/>
                </Grid>
                <Grid item style={{fontSize: 12}}>
                    {
                        post.tags.map((value, index) => {
                            return `${index !== 0 ? ',' : ''}${value}`
                        })
                    }
                </Grid>
            </Grid>
            <Grid container item spacing={0} xs={12}>
                <Grid item style={{marginRight: 24}}>
                    <img alt='post-image' className={classes.postImage} src={post.image_url}/>
                </Grid>
                <Grid item>
                    <Typography style={{fontFamily: 'Raleway', fontWeight: 300}}>
                        {post.body}
                    </Typography>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Post;
