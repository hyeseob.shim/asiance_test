import {Button, Grid} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles'

const useStyles = makeStyles({
    paginationButton: {
        borderRadius: 0,
        color: 'grey',
        fontFamily: 'Raleway',
        fontWeight: 600,
        minWidth: 'auto',
        width: '40px',
        '&:hover': {
            backgroundColor: 'white'
        }
    },
    // page
    paginationButtonActive: {
        borderRadius: 0,
        backgroundColor: 'black',
        color: 'white',
        minWidth: 'auto',
        width: '40px',
        '&:hover': {
            backgroundColor: 'black'
        }
    }
});

const Pagination = (props) => {
    const {currentPage, maxPage, minPage, setCurrentPage} = props;

    const classes = useStyles();

    const renderNumber = () => {
        const from = parseInt((currentPage - 1) / 5);
        return Array(5).fill(1).map((value, index) => {
            return (
                <Grid item key={index}>
                    <Button className={
                        (from * 5 + index + 1) === currentPage ? classes.paginationButtonActive : classes.paginationButton
                    } onClick={
                        (e) => setCurrentPage(from * 5 + index + 1)
                    }>
                        {from * 5 + index + 1}
                    </Button>
                </Grid>
            );
        });
    };

    return (
        <Grid container spacing={1} item xs={10}>
            <Grid item>
                <Button className={classes.paginationButton} onClick={(e) => setCurrentPage(1)}>
                    FIRST
                </Button>
            </Grid>
            <Grid item>
                <Button className={classes.paginationButton} onClick={(e) => {
                    const to = currentPage % 5 === 0 ? currentPage - 5 : currentPage - currentPage % 5;
                    to > 0 && setCurrentPage(to);
                }}>
                    PREV
                </Button>
            </Grid>
            {
                renderNumber()
            }
            <Grid item>
                <Button className={classes.paginationButton} onClick={(e) => {
                    const to = parseInt((currentPage - 1) / 5) + 1;
                    to * 5 + 1 <= maxPage && setCurrentPage(to * 5 + 1);
                }}>
                    NEXT
                </Button>
            </Grid>
            <Grid item>
                <Button className={classes.paginationButton} onClick={(e) => setCurrentPage(maxPage)}>
                    LAST
                </Button>
            </Grid>
        </Grid>
    );
};

export default Pagination;
