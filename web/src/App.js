import React, {useEffect, useState} from 'react';

import {AppBar, Grid, Tab, Tabs, Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';

import './App.css';

import Pagination from './components/Pagination';
import Post from './components/Post';

const useStyles = makeStyles({
    appBar: {
        backgroundColor: 'white',
        boxShadow: '0 0 black !important',
        fontFamily: 'Squada One',
        height: 'fit-content'
    },
    appBarTab: {
        minHeight: 'auto'
    },
    containerPosts: {
        marginBottom: '15px',
        marginTop: '120px'
    },
    copyright: {
        margin: '40px 0 20px 0'
    },
    root: {
        backgroundColor: 'rgba(0, 0, 0, 0.02)',
        minHeight: '100vh'
    },
    tabButton: {
        minHeight: 'auto',
        minWidth: 'auto',
        margin: '0 8px',
        padding: '0px 6px'
    },
    title: {
        fontFamily: 'Staatliches',
        fontSize: 30,
        fontWeight: 800,
        padding: '20px 0 2px 0',
    }
});

const App = () => {
    const classes = useStyles();

    const [authorList, setAuthorList] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [maxPage, setMaxPage] = useState(undefined);
    const [postList, setPostList] = useState(undefined);
    const [tabValue, setTabValue] = useState('POSTS');

    useEffect(function () {
        fetch('https://pxzyh6ynu1.execute-api.ap-northeast-2.amazonaws.com/beta/posts',
            {method: 'GET'}
        ).then(resp => {
                if (resp.status === 200) {
                    return resp.json();
                }
            }
        ).then(data => {
            setPostList(data);
            let maxPageValue = parseInt(data.length / 5);
            if (data.length % 5 > 1) {
                maxPageValue += 1;
            }
            setMaxPage(maxPageValue);
        });
        fetch('https://pxzyh6ynu1.execute-api.ap-northeast-2.amazonaws.com/beta/authors',
            {method: 'GET'}
        ).then(resp => {
            if (resp.status === 200) {
                return resp.json();
            }
        }).then(data => {
            setAuthorList(data);
        });
    }, []);

    const getAuthor = (author) => {
        let authorObject;
        authorList.map(value => {
            if (author === value.name) {
                authorObject = value;
            }
        })
        return authorObject || {name: '', role: '', avatar: '', location: ''};
    };

    return (
        <Grid className={classes.root} container justify='center'>
            <Grid item  xs={12}>
                <AppBar className={classes.appBar} color='transparent' position='absolute'>
                    <Typography align='center' className={classes.title} component='div'>
                        The Blog
                    </Typography>
                    <Tabs centered
                          className={classes.appBarTab}
                          indicatorColor='primary'
                          value={tabValue}>
                        <Tab className={classes.tabButton}
                             label='POSTS'
                             onClick={(e) => setTabValue('POSTS')}
                             style={{color: tabValue === 'POSTS' ? 'black' : 'grey', fontFamily: 'Raleway'}}
                             value='POSTS'/>
                        <Tab className={classes.tabButton}
                             label='ABOUT US'
                             onClick={(e) => setTabValue('ABOUT_US')}
                             style={{color: tabValue === 'ABOUT_US' ? 'black' : 'grey', fontFamily: 'Raleway'}}
                             value='ABOUT_US'/>
                    </Tabs>
                </AppBar>
            </Grid>
            {
                tabValue === 'POSTS' &&
                <>
                    <Grid className={classes.containerPosts} container item justify='center' xs={12}>
                        {
                            postList && Array(5).fill(1).map((value, index) => {
                                const post = postList[(currentPage - 1) * 5 + index];
                                const author = getAuthor(post.author);
                                return (<Post author={author} key={index} post={post}/>);
                            })
                        }
                    </Grid>
                    <Pagination currentPage={currentPage} maxPage={maxPage} setCurrentPage={setCurrentPage}/>
                </>
            }
            <Grid className={classes.copyright} item  xs={12}>
                <Typography align='center' style={{fontFamily: 'Raleway', fontSize: '12px'}}>
                    © 2004 - 2019 The blog. All rights reserved.
                </Typography>
            </Grid>
        </Grid>
    );
};

export default App;
