import datetime

def lambda_handler(event, context):
    result = []

    for i in range(100):
        i += 1
        result.append({
            "author": "author_{}".format(i % 20 + 1),
            "title": "post_title_{}".format(i),
            "body": "post_body_{}".format(i),
            "tags": ["tag_{}".format(j) for j in range(i % 10)],
            "image_url": "https://material-ui.com/static/images/grid-list/camera.jpg",
            "created_at": str(datetime.datetime.utcnow()),
            "updated_at": str(datetime.datetime.utcnow()),
        })

    return result