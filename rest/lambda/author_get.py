import datetime

def lambda_handler(event, context):
    result = [
    ]
    for i in range(20):
        i += 1
        result.append({
            "name": "author_{}".format(i),
            "role": "role_{}".format(i),
            "avatar": "https://material-ui.com/static/images/avatar/{}.jpg".format(i % 3 + 1),
            "location": "location_{}".format(i),
            "creataed_at": str(datetime.datetime.utcnow()),
            "updated_at": str(datetime.datetime.utcnow()),
        })
    return result
